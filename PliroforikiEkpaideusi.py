# -*- coding: utf-8 -*-
"""
Created on Sat Dec 25 17:04:37 2021

@author: Anastasia Kanakaki, Ioannis Velis, Thodoris Kalmanidis
"""
#import libraries
import pandas as pd
#from colorama import Fore, Style,Back
from datetime import datetime

# importing the required library
 
#import seaborn as sns
import os
import matplotlib.pyplot as plt
#create a variable dt to get random ms of current time
dt = datetime.now()
dt.microsecond
#read csv with questions
df = pd.read_excel (r'C:\\Users\\30694\\Downloads\\Book2.xlsx') 


#Take the first 10 lines from excel
df_1 = df.iloc[:9,:]
# and group them based on the efedriki erotisi flag
grouped1 = df_1.groupby(df_1.Efedrikes)
df_efedrikes1 = grouped1.get_group("E")
# do the same for the rest questions to create second quiz
df_2 = df.iloc[10:,:]
grouped2 = df_2.groupby(df_2.Efedrikes)
df_efedrikes2 = grouped2.get_group("E")

#function to print the barplots
def returnScores (score,plotdf,dataframe1):
    
    
#replace True False with 0.1 for the plotting
    plotdf.replace({False: 0, True: 1}, inplace=True)
# pandas dataframe barplot
    plotdf.plot(kind='bar',x='Thema',y='Boolean')
    return print("Your final score is "+str(score))

# main function for the quiz which takes 2 dataframes with main and side questions
def Quiz(dataframe1,dataframe2):
    score=0
# assign a new dataframe for the plotting and score
    plotdf = pd.DataFrame()
   # for every index and row in the dataframe print the question with the possible answers
    for index,row in dataframe1.iterrows():
      os.system('cls')
      print (row['Question'])
      print (row['Answer A'])
      print ( row['Answer B'])
      print ( row['Answer C'])
      print (row['Answer D'])
      print( row['Correct Answer'])
      print( row['Dummy Answer'])

      while True:
        
 # accept only answers if they are abcd else try again
           print("========== INPUT===========") 
           input_var = input("Enter something: ")
           print("========== INPUT===========") 
           if input_var.lower() not in ('a', 'b', 'c', 'd'):
              print("Not an appropriate choice.")
              
  
           else:
            break

# if you selected correct or worng answer get the appropriate comment and boolean value
      if (input_var==  row['Correct Answer']  ):
                print("------------------------------") 
    
                plotdf=plotdf.append({'Thema': row['Thema'], 'Boolean':True  }, ignore_index=True)
                

                print(row["Comment Correct"]+" "+row['Thema'])
                print("------------------------------") 
                score=score+1
  
      elif (input_var==  row['Not Understand Answer']  ):  
                     print("------------------------------") 
                     print(row['Comment Not Understand'] +" "+row['Thema'])
                     print("------------------------------") 
                     print("You need to study more using this material"+row['html'])
                     print("------------------------------") 
                     plotdf=plotdf.append({'Thema': row['Thema'], 'Boolean':False  }, ignore_index=True)
      elif (input_var==  row['Not Understand Well Answer']  ):
                     print("------------------------------") 
                     print(row['Comment Not Understand Well']+" " +row['Thema']) 
                     print("------------------------------") 
                     print("You need to study more using this material "+row['html'])
                     print("------------------------------") 
                     plotdf=plotdf.append({'Thema': row['Thema'], 'Boolean':False  }, ignore_index=True)
      #when the answer is dummy loop through another question set            
      elif (input_var==  row['Dummy Answer']  ):
                index1=index+1
         
          
                for index,row in dataframe2.iterrows():
                   if (index==index1): 
                            print (row['Question'])
                            print (row['Answer A'])
                            print (row['Answer B'])
                            print (row['Answer C'])
                            print (row['Answer D'])
              
                      
                            plotdf=plotdf.append({'Thema': row['Thema'], 'Boolean':False  }, ignore_index=True)
                    
                            
                    
                            while True:
                                  

                                   print("========== INPUT===========") 
                                   input_var = input("Enter something: ")
                                   print("========== INPUT===========") 
                                   if input_var.lower() not in ('a', 'b', 'c', 'd','e'):
                                     print("Not an appropriate choice.")
         
                                   else:
                                    break
        #age was successfully parsed!
        #we're ready to exit the loop.
                                               

                            if (len(input_var)==1 and input_var in ('a','b','c','d')):
                           
                               if (input_var==  row['Correct Answer']  ):
                                print("------------------------------")   
                                print(row['Comment Correct'] +" "+row['Thema'])
                                print("------------------------------") 
                                plotdf=plotdf.append({'Thema': row['Thema'], 'Boolean':True  }, ignore_index=True)
                                score=score+0.5
                            
                                break
                               else:
                                print("------------------------------") 
                                print("Wrong Answer")  
                                print("------------------------------") 
                                print("You need to study more using this material"+" "+row['Link'])
                                print("------------------------------") 
                                plotdf=plotdf.append({'Thema': row['Thema'], 'Boolean':False  }, ignore_index=True)
                                os.system('cls')
                                break
                  
              
      else: 
          print("Please type a or b or c or d")
          

      
      
      
      
    return returnScores (score,plotdf,dataframe1)
#print("Your final score is "+str(score))



#Based on the current time pick one from the two quiz 
# and pass them to the quiz     
if (dt.microsecond % 2 >0):
    df_1 = df.iloc[:9,:]
    grouped1 = df_1.groupby(df_1.Efedriki)
    df_efedrikes1 = grouped1.get_group("E")
    df_kanonikes1 = grouped1.get_group("N")
    print("Package 1")
    if (dt.microsecond % 2 >0):  
     Quiz( df_kanonikes1,  df_efedrikes1 )
    else:
     Quiz( df_efedrikes1 ,df_kanonikes1)    
else:
    df_2 = df.iloc[10:,:]
    grouped2 = df_2.groupby(df_2.Efedriki)
    df_efedrikes2 = grouped2.get_group("E")
    df_kanonikes2 = grouped2.get_group("N")
    if (dt.microsecond % 2 >0):  
     Quiz(df_efedrikes2 ,df_kanonikes2)
    else:
     Quiz( df_kanonikes2,  df_efedrikes2 )
    print("Package 2")
    
    